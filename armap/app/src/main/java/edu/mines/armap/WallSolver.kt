package edu.mines.armap

import android.util.Log
import com.google.ar.core.Pose
import koma.end
import koma.extensions.reshape
import koma.internal.default.generated.matrix.DefaultDoubleMatrix
import koma.internal.default.generated.matrix.DefaultDoubleMatrixFactory
import koma.mat
import koma.matrix.Matrix
import koma.matrix.ejml.EJMLMatrix
import java.lang.Exception
import java.lang.Math.sqrt

class WallSolver {
    private var elems: DoubleArray = DoubleArray(0)
    private var solver: Matrix<Double>? = null

    fun addWall(px: Double, py: Double, _nx: Double, _ny: Double) {
        val mag = sqrt(_nx * _nx + _ny * _ny)
        val nx = _nx / mag
        val ny = _ny / mag
        val c = nx * px + ny * py
        elems += doubleArrayOf(nx, ny, -c)
    }

    fun setupBuiltin() {
        elems = doubleArrayOf(0.144, -0.246667, 0.082312, 0.398667, 0.00333333, 0.0346751, 0.016, 0.16, 0.0351147, -0.323333, -0.0, 0.129333)
        updateSolver()
//        solver = mat[0.354069, 1.69602, 0.510522, -0.818663 end
//                -2.37804, 1.16645, 2.55956, 0.505795 end
//                2.05032, 3.01076, 3.09819, 4.77869]
    }

    fun updateSolver(): Boolean {
        if (elems.size < 9) {
            return false
        }
        val mtx = DefaultDoubleMatrixFactory()
                .create(elems)
                .reshape(elems.size / 3, 3)
        try {
            solver = (mtx.T * mtx).inv() * mtx.T
        } catch (e: Exception) {
            return false
        }
        return true
    }

    fun solve(dists: DoubleArray): Pair<Double, Double>? {
        val solver = solver ?: return null
        if (dists.size != solver.numCols()) { return null }
        val params = solver * DefaultDoubleMatrixFactory().create(dists).T
        val scale = params.getDouble(2, 0)
        return Pair(params.getDouble(0, 0) / scale, params.getDouble(1, 0) / scale)
    }

    fun numMarks(): Int {
        return elems.size / 3
    }
}

fun doublesToFloats(input: FloatArray): DoubleArray {
    val output = DoubleArray(input.size)
    for (i in 0 until input.size) {
        output[i] = input[i].toDouble()
    }
    return output
}

fun distanceAlong(pos: Pose, ray: Pose): Double {
    return distanceAlong(
            doublesToFloats(pos.translation),
            doublesToFloats(ray.translation),
            doublesToFloats(ray.yAxis))
}

fun distanceAlong(pos: DoubleArray, origin: DoubleArray, ray: DoubleArray): Double {
    var dot = 0.0
    for (i in 0 until pos.size) {
        val diff = pos[i] - origin[i]
        dot += diff * ray[i]
    }
    return dot
}