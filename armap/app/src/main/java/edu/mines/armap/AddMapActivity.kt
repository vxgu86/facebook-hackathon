package edu.mines.armap

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import kotlinx.android.synthetic.main.activity_add_map.*
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Files
import java.nio.file.Path


class AddMapActivity : AppCompatActivity() {

    private val CAMERA_PIC_REQUEST: Int = 1313
    private var realFile: File? = null
    private var uri: Uri? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Optional: Hide the status bar at the top of the window
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // Set the content view and get references to our views
        setContentView(R.layout.activity_add_map)

        cancelBtn.setOnClickListener {
            finish()
        }

        addBtn.setOnClickListener {
            val intent = Intent()
            intent.putExtra("filename", realFile.toString())
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        takePictureButton.setOnClickListener {
            realFile = File(
                    Environment.getExternalStorageDirectory().toString() +
                    File.separator + "armap" + File.separator + map_name.text)
            uri = FileProvider.getUriForFile(
                    applicationContext,
                    BuildConfig.APPLICATION_ID + ".provider",
                    realFile as File)

            val intent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)

            startActivityForResult(intent, CAMERA_PIC_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CAMERA_PIC_REQUEST) {
            val bm = MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, uri)
            realFile = File(Environment.getExternalStorageDirectory().toString() +
                    File.separator + "armap" + File.separator + map_name.text)
            val out = FileOutputStream(realFile)
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out)
            bm.recycle()
        }
    }
}
