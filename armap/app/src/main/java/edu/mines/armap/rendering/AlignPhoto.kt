package edu.mines.armap.rendering

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import edu.mines.armap.MainActivity
import edu.mines.armap.R
import kotlinx.android.synthetic.main.activity_photo_selector.*
import kotlinx.android.synthetic.main.fragment_align_photo.*
import java.io.File
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import edu.mines.armap.helpers.LinePhotoView


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [AlignPhoto.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [AlignPhoto.newInstance] factory method to
 * create an instance of this fragment.
     *
     */
class AlignPhoto : Fragment() {
    // TODO: Rename and change types of parameters
    private var path: String? = "path"
//    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    var bgImg: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
//            path = it.getString("path")
//            param2 = it.getString(ARG_PARAM2)
        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_align_photo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var path = (activity as MainActivity).fileName()

        Log.i("info_app_frag", "Path is: " + path)

        bgImg = if (path!![0] == '/')
            MediaStore.Images.Media.getBitmap(activity!!.contentResolver,
                    Uri.fromFile(File(path)))
        else
            BitmapFactory.decodeStream(activity!!.assets.open(path))

        var image = getView()!!.findViewById<View>(R.id.photo_view) as LinePhotoView

        image.setImageBitmap(bgImg)

        image.getController().getSettings()
                .setRotationEnabled(true)
//                .setOverscrollDistance(10000f, 10000f)
                .disableBounds()

        finished_button.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                var activity = activity as MainActivity
                var view = photo_view as LinePhotoView

                activity.addMatrix(view.imageMatrix)

            }
        })
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

//    override fun onAttach(context: Context) {
//        super.onAttach(context)
//        if (context is OnFragmentInteractionListener) {
//            listener = context
//        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
//        }
//    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AlignPhoto.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String): Fragment =
                AlignPhoto().apply {
                    arguments = Bundle().apply {
                        putString("path", param1)
                    }
                }
    }
}
