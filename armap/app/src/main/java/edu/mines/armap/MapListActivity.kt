package edu.mines.armap

import android.Manifest
import android.os.Bundle
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import edu.mines.armap.models.MapModel
import kotlinx.android.synthetic.main.activity_map_list.*
import kotlinx.android.synthetic.main.map_item.view.*
import java.io.File
import edu.mines.armap.PhotoSelector

class MapListActivity : AppCompatActivity() {
    val MY_PERMISSIONS_REQUEST_READ_CONTACTS: Int = 0

    var REQUEST_MATRIX_INTENT = 1

    var mapList: List<MapModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_list)

        populateMapList()
        map_list.layoutManager = LinearLayoutManager(this)
        map_list.hasFixedSize()
        map_list.adapter = MapAdapter(mapList) { partItem : MapModel -> mapItemClicked(partItem) }

        add_map_button.setOnClickListener { view: View -> addMapButtonClicked(view)}
    }

    private fun populateMapList() {
        val newList = ArrayList<MapModel>()
        newList.add(MapModel("Facebook HQ Building 18", "models/map.png"))

        val dir = File(Environment.getExternalStorageDirectory().toString() +
                        File.separator + "armap")
        dir.mkdirs()
        val files = dir.listFiles()

        if (files != null) {
            for (file in files) {
                if (file.isDirectory()) continue
                newList.add(MapModel(file.nameWithoutExtension, file.toString()))
            }
        }
        mapList = newList
    }

    private fun addMapButtonClicked(view: View) {
        // Launch the Add Map activity.
        val showDetailActivityIntent = Intent(this, AddMapActivity::class.java)
        startActivityForResult(showDetailActivityIntent, 1)
    }

    private fun openMap(filename: String) {
        Log.i("opening", filename)

        // Launch the Main activity.
        val showDetailActivityIntent = Intent(this, MainActivity::class.java)
        showDetailActivityIntent.putExtra("map_filename", filename)
        startActivity(showDetailActivityIntent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {
            openMap(data.extras.get("filename") as String)
        }
    }

    private fun mapItemClicked(mapItem : MapModel) {
        // TODO: FIX THIS
        openMap(mapItem.imageUri)
    }
}

class MapAdapter (private val mapList: List<MapModel>, private val clickListener: (MapModel) -> Unit) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // LayoutInflater: takes ID from layout defined in XML.
        // Instantiates the layout XML into corresponding View objects.
        // Use context from main app -> also supplies theme layout values!
        val inflater = LayoutInflater.from(parent.context)
        // Inflate XML. Last parameter: don't immediately attach new view to the parent view group
        val view = inflater.inflate(R.layout.map_item, parent, false)
        return MapViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        // Populate ViewHolder with data that corresponds to the position in the list
        // which we are told to load
        (holder as MapViewHolder).bind(mapList[position], clickListener)
    }

    override fun getItemCount() = mapList.size

    class MapViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(part: MapModel, clickListener: (MapModel) -> Unit) {
            itemView.map_name.text = part.name
            itemView.setOnClickListener { clickListener(part)}
        }
    }
}
